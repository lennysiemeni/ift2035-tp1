-- Auteurs
-- Mehran ASADI : 1047847
-- Lenny SIEMENI Matricule : 1055234

module Eval where

import qualified Data.Map as M
import Parseur


--------------------------------------------------------------------------------
-- Les expressions et la traduction de Sexp en expression (Exp)
--------------------------------------------------------------------------------

-- type a = b indique que a est un synonyme de b
-- il ne s'agit pas vraiment d'un nouveau type
type Type = Symbol
type Constructor = Symbol
type DataConstructor = (Constructor, [Type])

type CasePattern = (Symbol, [Symbol], Exp)

data Mutability = Constant
                | Mutable
                deriving (Eq, Show)
                
data Scope = Lexical
           | Dynamic
           deriving (Eq, Show)

-- Les expressions du langages ouf
-- Vous n'avez pas à modifier ce datatype
data Exp = EInt Int
         | EVar Symbol
         | EDefine Symbol Exp
         | EApp Exp Exp
         | ELam Symbol Exp
         | ESet Symbol Exp
         | ELet [(Symbol, Exp)] Exp
         | EData Type [DataConstructor]
         | ECase Exp [CasePattern]
         | EOufScope Symbol Scope
         | EOufMutability Symbol Mutability
         deriving (Eq, Show)

type Error = String

reservedKeywords :: [Symbol]
reservedKeywords = ["lambda", "let", "case", "data", "define", "set", "ouf", "Error"]


var2Exp :: Sexp -> Either Error Exp
var2Exp (SSym ident) = Right $ EVar ident
var2Exp _ = Left "Doit être un identificateur"

var2Symbol :: Sexp -> Either Error Symbol
var2Symbol (SSym ident) = Right ident
var2Symbol _ = Left "Doit être un identificateur"

-- Vous devez compléter une partie cette fonction
sexp2Exp :: Sexp -> Either Error Exp
sexp2Exp (SNum x) = Right $ EInt x
sexp2Exp (SSym ident) =
  if ident `elem` reservedKeywords
  then Left $ ident ++ " is a reserved keyword"
  else Right $ EVar ident
sexp2Exp (SList ls@((SSym s) : xs)) | s `elem` reservedKeywords
  = specialForm2Exp ls 
sexp2Exp (SList (func : [])) =
  Left "Function application must provide at least one parameter"

-- Il faut écrire le cas pour les fonctions
--sexp2Exp (SList (func : args)) = Left "Vous devez compléter cette partie"
sexp2Exp (SList (func : a1: args)) = do
                    fexp <- sexp2Exp func
                    a1exp <- sexp2Exp a1
                    argsexp <- sequence $ applique sexp2Exp args
                    --Right $ processApp Params (EApp fexp a1 exp) argssexp
                    return $ foldl (\args1 args2 -> (EApp args1 args2)) (EApp fexp a1exp) (argsexp)

sexp2Exp _ = Left "Erreur de syntaxe"

-- Il faut compléter cette fonction qui gère
-- toutes les formes spéciales (lambda, let ...)
specialForm2Exp :: [Sexp] -> Either Error Exp
specialForm2Exp ((SSym "lambda") :
                 (SList []) :
                 _ : []) = Left "Syntax Error : No parameter"
  
specialForm2Exp ((SSym "lambda") :
                 (SList params) :
                 body :
                 []) = do
  body' <- sexp2Exp body
  params' <- sequence  $ reverse $ map var2Symbol params
  return $ foldl (\b s -> ELam s b)
                 (ELam (head params') body')
                 (tail params')

-- On ajoute le special keyword define
specialForm2Exp ((SSym "define") : variable: corps:[]) = do
                        variable1 <- var2Symbol variable
                        corps1 <- sexp2Exp corps
                        return (EDefine variable1 corps1)

-- On ajoute le special keyword "set"
specialForm2Exp ((SSym "set"): (SSym variable): corps: []) = do
                          exp' <- sexp2Exp corps
                          return $ ESet variable exp'

-- On ajoute le special keyword let
specialForm2Exp ((SSym "let") : (SList expression): corps: []) = do
                        symbolExp <- sequence $ applique symboleExpression expression
                        corps1 <- sexp2Exp corps
                        case symbolExp of
                            (variable:donnee) -> return (ELet (variable:donnee) corps1)
                            _ -> return (ELet [] corps1)

-- On ajoute le special keyword case
specialForm2Exp ((SSym "case") : (variable) : (SList expression):[]) = do
  variableExpression <- sequence $ applique symbolExpression expression
  expressions <- var2Exp variable
  return (ECase expressions variableExpression)
-- On ajoute le special keyword data 
specialForm2Exp ((SSym "data") : t1 : constructeurs) = do 
                    t2 <- var2Symbol t1
                    constructeurs1 <- sequence $ applique listConstructeur constructeurs
                    return (EData t2 constructeurs1)

specialForm2Exp _ = Left "Syntax Error : Unknown special form"

symboleExpression :: Sexp -> Either Error (Symbol,Exp)
symboleExpression (SList (variable:donnee:[])) = do
 variable1 <- var2Symbol variable
 donnee1 <- sexp2Exp donnee
 return $ (variable1, donnee1)
applique f [] = []
applique f (x:xs) = f x:applique f xs
listConstructeur (SList expressions) = do
  listConstructeurs <- sequence $ applique var2Symbol expressions
  return ((listConstructeurs !! 0),(drop 1 listConstructeurs))

symbolExpression (SList ((SList construct):expression:[])) = do
  expressions <- sexp2Exp expression
  variables <- sequence $ applique var2Symbol construct
  return ((variables !! 0), (drop 1 variables), expressions)
--------------------------------------------------------------------------------
-- L'évaluation
--------------------------------------------------------------------------------

-- Les valeurs retournées par l'évaluateur
-- Vous n'avez pas à modifier ce datatype
data Value = VInt Int
           | VLam Symbol Exp LexicalEnv
           | VPrim (Value -> Value)
           | VData Type Type [Value]
           | VUnit

instance Show Value where
  show (VInt n) = show n
  show (VData t c d) = "VData " ++ t ++ " (" ++
    (unwords $ show c : map show d) ++ ")"
  show VUnit = "VUnit"
  show (VPrim _) = "<primitive>"
  show (VLam s e env) = "VLamda [" ++ s ++ (unwords [",", show e, ",", show env])
    ++ "]"
  
instance Eq Value where
  (VInt n1) == (VInt n2) = n1 == n2
  VUnit == VUnit = True
  (VData t1 c1 d1) == (VData t2 c2 d2) =
     t1 == t2 && c1 == c2 && d1 == d2 
  -- Functions and primitives are not comparable
  _ == _ = False

-- Un environnement pour portée lexicale
-- Vous n'avez pas à modifier ce datatype
type LexicalEnv = [(Symbol, Value)]

-- L'environnement. Au début, comme celui de la portée lexicale
-- Vous devrez modifier ce type pour la portée dynamique
-- et les instructions ouf
type Env = LexicalEnv

-- lookup de la librairie standard utilise Maybe
-- au lieu de Either
lookup2 :: [(Symbol, a)] -> Symbol -> Either Error a
lookup2 [] sym = Left $ "Not in scope " ++ sym
lookup2 ((s, v) : _) sym | s == sym = Right v
lookup2 (_ : xs) sym = lookup2 xs sym

-- Recherche un identificateur dans l'environnement
lookupVar :: Env -> Symbol -> Either Error Value
lookupVar = lookup2

-- Ajoute une variable dans l'environnement
insertVar :: Env -> Symbol -> Value -> Env
insertVar e s v =  (s, v) : e

-- Insert plusieurs variables dans l'environnement
-- La première variable de la liste est la dernière insérée
insertVars :: Env -> [(Symbol, Value)] -> Env
insertVars env xs = foldr (\(s, v) env -> insertVar env s v) env xs

primDef :: [(Symbol, Value)]
primDef = [("+", prim (+)),
           ("-", prim (-)),
           ("*", prim (*))]
  where prim op =
          VPrim (\ (VInt x) -> VPrim (\ (VInt y) -> VInt (x `op` y)))

envEmpty :: Env
envEmpty = []

env0 :: Env
env0 = insertVars envEmpty primDef


-- L'évaluateur au niveau global
-- L'évaluateur retourne une valeur et un environnement mis à jour
-- L'environnement mis à jour est utile pour de nouvelles définitions
-- avec define ou data ou lorsque les variables sont
-- modifiées par set par exemple.
evalGlobal :: Env -> Exp -> Either Error (Env, Value)
evalGlobal env (EDefine sym exp) = do
  (env', value) <- eval env exp
  return $ ((insertVar env sym value), value) 
--evalGlobal env (EData t cs) = Left "evalGlobal env (EData t cs)"
-- L'environnement genere par un EData
evalGlobal env (EData t constructs) = Right $ ((insertVars env (applique (\(val,sym)-> (val, (VData t val (map (\y -> VUnit) sym)))) constructs)), VInt 0)
evalGlobal env e = eval env e -- Autre que Define et Data, eval prend le relais

-- L'évaluateur pour les expressions
eval :: Env -> Exp -> Either Error (Env, Value)
eval _ (EDefine _ _) = Left $ "Define must be a top level form"
eval _ (EData _ _) = Left $ "Data must be a top level form"
eval env (EInt x) = Right (env, VInt x)
eval env (EVar sym) = do
  v <- lookupVar env sym
  return (env, v)

-- Fonction de l'evaluation pour le set
eval env (ESet sym e) = Right $ (env, (VLam sym e env))

-- Fonction de l'evaluation pour une lambda expression
eval env (ELam sym body) = Right $ (env, (VLam sym body env))

-- Fonction de l'evaluation pour l'application de fonction
eval env (EApp func arg) = do
                        (env1,fonctions) <- eval env func
                        (env2,argument) <- eval env arg
                        case fonctions of
                            VPrim fonction -> return (env, (fonction argument))
                            VLam variable corps environnement -> do 
                              (envTemp, resultat) <- eval (insertVar environnement variable argument) corps
                              return (env, resultat)
                            (VData type1 type2 listValue) -> return (env, (VData type1 type2 (nonUnite ++ (argument:[]) ++ isUnite))) where
                                                        nonUnite = testerNonUnite listValue
                                                        (_:isUnite) = testerEstUnite listValue
                            _ -> Left "Erreur dans fonctions"
                            
-- Fonction de l'evaluation pour le let
eval env (ELet decls e) = do
  customEnv <- recursiveInsertion env decls
  (envTemp, donnee) <- eval customEnv e
  return (env, donnee)
 where
 recursiveInsertion env ((variable,donnee):customEnv) = do
  (envTemp, donnees) <- eval env donnee
  newEnv <- recursiveInsertion (insertVar env variable donnees ) customEnv
  return newEnv
 recursiveInsertion env [] = Right env
  
-- Fonction de l'evaluation pour le case
eval env (ECase e patterns) = do
  (envTemp, dataList) <- eval env e
  case dataList of 
    (VData _ types constructeur) ->
      case semblable of 
        (_,constructeurs,expression):_ -> do 
          (_, val) <- eval (insertVars env (verifierPaire constructeurs constructeur)) expression
          return (env, val) 
        [] -> Left "Erreur dans case"
      where
        verifierSemblable = estSemblable (constructeur,types)
        semblable = verifierList verifierSemblable patterns
    _ -> Left "Erreur dans case"
eval env (EOufScope sym scope) = Left "eval env (EOufScope sym scope)"

eval env (EOufMutability ident mutability) = Left "eval env (EOufMutability ident mutability)"

pasUnite unite = unite /= VUnit
estUnite unite = unite == VUnit

verifierList :: (a -> Bool) -> [a] -> [a]
verifierList p (x:xs) | p x = x : verifierList p xs
                      | otherwise = verifierList p xs
verifierList _ [] = []
testerNonUnite = verifierList pasUnite
testerEstUnite = verifierList estUnite
estSemblable (constructeur,types) (symbole,listSymbol,_) = symbole == types && length listSymbol == length constructeur
verifierPaire :: [a] -> [b] -> [(a,b)]
verifierPaire (a:as) (b:bs) = (a,b) : verifierPaire as bs
verifierPaire _ _ = []