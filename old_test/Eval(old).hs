module Eval where

import qualified Data.Map as M
import Parseur


--------------------------------------------------------------------------------
-- Les expressions et la traduction de Sexp en expression (Exp)
--------------------------------------------------------------------------------

type Type = Symbol
type Constructor = Symbol
type DataConstructor = (Constructor, [Type])

type CasePattern = (Symbol, [Symbol], Exp)

data Mutability = Constant
                | Mutable
                deriving (Eq, Show)
                
data Scope = Lexical
           | Dynamics
           deriving (Eq, Show)

-- Les expressions du langages ouf
-- Vous n'avez pas à modifier ce datatype
data Exp = EInt Int
         | EVar Symbol
         | EDefine Symbol Exp
         | EApp Exp Exp
         | ELam Symbol Exp
         | ESet Symbol Exp
         | ELet [(Symbol, Exp)] Exp
         | EData Type [DataConstructor]
         | ECase Exp [CasePattern]
         | EOufScope Symbol Scope
         | EOufMutability Symbol Mutability
         deriving (Eq, Show)

type Error = String

reservedKeywords :: [Symbol]
reservedKeywords = ["lambda", "let", "case", "data", "define", "set", "ouf", "scope", "mutability", "Error"]


var2Exp :: Sexp -> Either Error Exp
var2Exp (SSym ident) = Right $ EVar ident
var2Exp _ = Left "Doit être un identificateur"

var2Symbol :: Sexp -> Either Error Symbol
var2Symbol (SSym ident) = Right ident
var2Symbol _ = Left "Doit être un identificateur"

sexp2Exp :: Sexp -> Either Error Exp
sexp2Exp (SNum x) = Right $ EInt x
sexp2Exp (SSym ident) =
  if ident `elem` reservedKeywords
  then Left $ ident ++ " is a reserved keyword"
  else Right $ EVar ident
sexp2Exp (SList ls@((SSym s) : xs)) | s `elem` reservedKeywords
  = specialForm2Exp ls 
sexp2Exp (SList (func : [])) =
  Left "Function application must provide at least one parameter"

-- Il faut écrire le cas pour les fonctions
sexp2Exp (SList (func : args)) = do 
                func' <- sexp2Exp func 
                args' <- sequence $ map sexp2Exp args
                return $ foldl (\accu arg -> (EApp accu arg)) 
                               (EApp func' (head args')) 
                               (tail args')

sexp2Exp _ = Left "Erreur de syntaxe"

specialForm2Exp :: [Sexp] -> Either Error Exp
specialForm2Exp ((SSym "lambda") :
                 (SList []) :
                 _ : []) = Left "Syntax Error : No parameter"
  
specialForm2Exp ((SSym "lambda") :
                 (SList params) :
                 body :
                 []) = do
  body' <- sexp2Exp body
  params' <- sequence  $ reverse $ map var2Symbol params -- comme on peut avoir plusieurs var
  return $ foldl (\b s -> ELam s b)
                 (ELam (head params') body')
                 (tail params')

-- On ajoute le special keyword "set"
specialForm2Exp ((SSym "set"): (SSym var): body: []) = do
  exp' <- sexp2Exp body
  return $ ESet var exp'

-- On ajoute le special keyword define
specialForm2Exp ((SSym "define") : (var): body:[]) = do
  body' <- sexp2Exp body
  var' <- var2Symbol var
  return (EDefine var' body')

-- On ajoute le special keyword let
specialForm2Exp ((SSym "let") : (SList params): body: []) = do
  body' <- sexp2Exp body
  params' <- sequence $ map (\(SList (x:y:[])) -> do
      x' <- var2Symbol x
      y' <- sexp2Exp y
      return (x',y')) params
  case params' of
      (a:params'') -> return (ELet (a:params'') body')
      _ -> return (ELet [] body')

-- On ajoute le special keyword case
specialForm2Exp ((SSym "case") : (var) : (SList branch):[]) = do
  expr <- var2Exp var
  branch' <- sequence $ map (\(SList ((SList construct):ex:[])) -> do
    exp' <- sexp2Exp ex
    id <- sequence $ map var2Symbol construct
    return ((head id), (tail id), exp')) branch
  return (ECase expr branch')

-- On ajoute le special keyword data 
specialForm2Exp ((SSym "data") : t1 : constructs) = do 
  t1' <- var2Symbol t1
  constructs' <- sequence $ map (\(SList t2) -> do --derivation de contructeur::=(type+)
    t2' <- sequence $ map var2Symbol t2 -- appliquer recursivement var2Symbol sur les type+
    return ((head t2'),(tail t2'))) 
    return (t2)
    (constructs)
  return (EData t2' constructs')


--                                                    specialForm2Exp ((SSym "data") : t1 : constructs) = do 
--                                                      t1' <- var2Symbol t1
--                                                      constructs' <- sequence $ map (\(SList t2) -> do --derivation de contructeur::=(type+)
--                                                        t2' <- sequence $ map var2Symbol t2 -- appliquer recursivement var2Symbol sur les type+
--                                                        return ((head t2'),(tail t2'))) 
--                                                        (constructs)
--                                                      return (EData typ' constructs')

-- On essaie d'implemanter la syntaxe ouf!
specialForm2Exp ((SSym "ouf") :
                 (SSym "scope") :
                 var : 
                 (SSym "lexical") : []) = do 
                                   var' <- var2Symbol var 
                                   return (EOufScope var' Lexical)
specialForm2Exp  ((SSym "ouf") :
                 (SSym "scope") :
                 var : 
                 (SSym "dynamic") : []) = do 
                                   var' <- var2Symbol var 
                                   return (EOufScope var' Dynamics)

specialForm2Exp ((SSym "ouf") :
                 (SSym "mutability") :
                 (var) :
                 (mutableState) : []) = Left "TODO"


specialForm2Exp _ = Left "Syntax Error : Unknown special form"

--------------------------------------------------------------------------------
-- L'évaluation
--------------------------------------------------------------------------------

-- Les valeurs retournées par l'évaluateur
-- Vous n'avez pas à modifier ce datatype
data Value = VInt Int
           | VLam Symbol Exp LexicalEnv
           | VPrim (Value -> Value)
           | VData Type Type [Value]
           | VUnit

instance Show Value where
  show (VInt n) = show n
  show (VData t c d) = "VData " ++ t ++ " (" ++
    (unwords $ show c : map show d) ++ ")"
  show VUnit = "VUnit"
  show (VPrim _) = "<primitive>"
  show (VLam s e env) = "VLamda [" ++ s ++ (unwords [",", show e, ",", show env])
    ++ "]"
  
instance Eq Value where
  (VInt n1) == (VInt n2) = n1 == n2
  VUnit == VUnit = True
  (VData t1 c1 d1) == (VData t2 c2 d2) =
     t1 == t2 && c1 == c2 && d1 == d2 
  -- Functions and primitives are not comparable
  _ == _ = False

-- Un environnement pour portée lexicale
-- Vous n'avez pas à modifier ce datatype
type LexicalEnv = [(Symbol, Value)]

-- Modifications des environnements pour la portee dynamique
type DynamicEnv = [(Symbol, Value)]
type Env = (DynamicEnv, LexicalEnv)

-- Adaptation de l'environnement pour la syntaxe ouf!
type TypeEnv = [(Type, [Symbol])]

-- lookup de la librairie standard utilise Maybe
-- au lieu de Either
lookup2 :: [(Symbol, a)] -> Symbol -> Either Error a
lookup2 [] sym = Left $ "Not in scope " ++ sym
lookup2 ((s, v) : _) sym | s == sym = Right v
lookup2 (_ : xs) sym = lookup2 xs sym

-- Recherche un identificateur dans l'environnement
lookupVar :: Env -> Symbol -> Either Error Value
lookupVar (dyn, lex) symb = do 
            let dynLookup = lookup2 dyn symb
            case dynLookup of
                (Right _) -> dynLookup
                _ -> lookup2 lex symb

-- Ajoute une variable dans l'environnement
insertVar :: Env -> Symbol -> Value -> Env
insertVar e s v = do
  let (dyn, lex) = e
  let e' = insertLexicalVar (dyn, lex) s v
  let dynLookup = lookup2 dyn s
  case dynLookup of 
    (Right _) -> insertDynamicVar e' s v
    _ -> e'

-- Inserer plusieurs variables dans l'environnement
-- La première variable de la liste est la dernière insérée
insertVars :: Env -> [(Symbol, Value)] -> Env
insertVars env xs = foldr (\(s, v) env -> insertVar env s v) env xs

-- Inserer une variable dans l'environnement lexical
insertLexicalVar :: Env -> Symbol -> Value -> Env 
insertLexicalVar (e, e') s v =  (e, ((s, v) : e'))

-- Inserer une variable dans l'environnement dynamique
insertDynamicVar :: Env -> Symbol -> Value -> Env
insertDynamicVar (e, e') s v = (((s, v) : e), e')

primDef :: [(Symbol, Value)]
primDef = [("+", prim (+)),
           ("-", prim (-)),
           ("*", prim (*))]
  where prim op =
          VPrim (\ (VInt x) -> VPrim (\ (VInt y) -> VInt (x `op` y)))

envEmpty :: Env
envEmpty = ([], [])

env0 :: Env
env0 = insertVars envEmpty primDef


-- L'évaluateur au niveau global
-- L'évaluateur retourne une valeur et un environnement mis à jour
-- L'environnement mis à jour est utile pour de nouvelles définitions
-- avec define ou data ou lorsque les variables sont
-- modifiées par set par exemple.
evalGlobal :: Env -> Exp -> Either Error (Env, Value)
evalGlobal env (EDefine s e) = do
  (e, v) <- eval env e
  return $ ((insertVar env s v), v)
   
-- L'environnement genere par un EData
evalGlobal env (EData t constructs) = Right $ ((insertVars env (map (\(val,sym)-> (val, (VData t val (map (\y -> VUnit) sym)))) constructs)), VInt 0)
evalGlobal env e = eval env e -- Autre que Define et Data, eval prend le relais

-- L'evaluateur pour les expressions
eval :: Env -> Exp -> Either Error (Env, Value)
eval _ (EDefine _ _) = Left $ "Define must be a top level form"
eval _ (EData _ _) = Left $ "Data must be a top level form"
eval env (EInt x) = Right (env, VInt x)
eval env (EVar sym) = do
  v <- lookupVar env sym
  return (env, v)

eval env (ESet sym e) = Left "Vous devez compléter cette partie"

-- Evaluation de l'environnement de l'application de fonction
eval env (EApp func arg) = do
  (_ , func') <- eval env func
  (_, arg') <- eval env arg
  case func' of 
  -- primitives
    (VPrim primFunc) -> return $ (env, (primFunc arg'))
  -- Evaluation d'une lambda expression
    (VLam symb body env') -> do 
      (_, lambdaExpr) <- eval (insertVar ((fst env), env') symb arg') body
      return (env, lambdaExpr)
  
  -- dataTypes
  -- TODO TypeEnv
    (VData t1 t2 constructs) -> let testEqual = (\x -> if x == VUnit then False else True)
                                    prec = filter testEqual constructs
                                    (l:last') = filter (not.testEqual) constructs
                                in return (env, (VData t1 t2 (prec ++ (arg':[]) ++ last')))
    _ -> Left "Too many parameters"

eval env (ELet decls e) = do 
  newEnv <- insertLetDecls env decls
  (_, val) <- eval (newEnv) e 
  return (env, val)

-- force Type dans l'env 
eval env (ECase e patterns) = do
  (_, dataList) <- eval env e
  case dataList of 
    (VData t1 t2 constructs) -> do 
      let testEqual = (\(c,a,b) -> if (((length a) == (length constructs)) && (c == t2)) then True else False) 
      let match = filter testEqual patterns
      case match of 
        [] -> Left "No default expression behaviour defined in case expression"
        (a,b,c):d -> do 
          (_, val) <- eval (insertVars env (zip b constructs)) c
          return (env, val) 
    _ -> Left "Case expression is ill formed"

eval env (EOufScope sym scope) = do 
  case scope of 
-- on cherche la valeur actuelle dans l'environement lexical; ouf dynamic donne la valeur lexicale a la variable
    (Dynamics) -> do 
      let val = lookupVar env sym
      case val of
          (Right val') -> return (insertDynamicVar env sym val', VInt 1)
          _ -> return (insertDynamicVar env sym (VUnit), VInt 2)
    (Lexical) -> do
      let (dyn, lex) = env
      let dyn' = filter (\(s, v) -> if s == sym then False else True) dyn
      return ((dyn', lex), VInt 0)
    
eval env (EOufMutability ident mutability) = Left "Vous devez compléter cette partie"

eval (dynEnv, lexEnv) (ELam sym body) = Right $ ((dynEnv, lexEnv), (VLam sym body lexEnv))

insertLetDecls :: Env -> [(Symbol, Exp)] -> Either Error Env
insertLetDecls env [] = Right $ env
insertLetDecls env ((a,b):xs) = do 
                                (_, b') <- eval env b
                                res <- insertLetDecls (insertVar env a b' ) xs
                                return res